import 'package:flutter/material.dart';
import 'package:meal_app2/models/meal.dart';

import '../dummy.dart';
class MealPR with ChangeNotifier{

  Map<String, bool> filters = {
    'gluten':false,
    'lactose':false,
    'vegan':false,
    'vegetarian':false,
  };
  List<Meal> availableMeal = DUMMY_MEALS;
  List<Meal> favoritesMeal = [];
  void setFilters(Map<String, bool> _filterData){
    filters = _filterData;

    availableMeal = DUMMY_MEALS.where((meal){
      if(filters['gluten']! && !meal.isGlutenFree){
        return false;
      }
      if(filters['lactose']! && !meal.isGlutenFree){
        return false;
      }
      if(filters['vegan']! && !meal.isGlutenFree){
        return false;
      }
      if(filters['vegetarian']! && !meal.isGlutenFree){
        return false;
      }
      return true;
    }).toList();
    notifyListeners();
  }
  bool isMealFavorite = false;
  void toggleFavorite(String mealId){
    final existingIndex = favoritesMeal.indexWhere((meal) => meal.id == mealId );

    if(existingIndex>=0){
      favoritesMeal.removeAt(existingIndex);
    }
    else{
      favoritesMeal.add(DUMMY_MEALS.firstWhere((meal) => meal.id == mealId));
    }
    isMealFavorite= favoritesMeal.any((meal) => meal.id == mealId);
    notifyListeners();
  }

}