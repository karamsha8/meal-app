import 'package:flutter/material.dart';
import 'package:meal_app2/dummy.dart';
import 'package:meal_app2/models/meal.dart';
import 'package:meal_app2/providers/meal_provider.dart';
import 'package:meal_app2/screen/categories_meal_screen.dart';
import 'package:meal_app2/screen/categories_screen.dart';
import 'package:meal_app2/screen/favorites_screen.dart';
import 'package:meal_app2/screen/filters_screen.dart';
import 'package:meal_app2/screen/meal_detail_screen.dart';
import 'package:meal_app2/screen/tab_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(ChangeNotifierProvider<MealPR>(
      create: (ctx) => MealPR(),
      child: MyApp(),
    ));

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: TabScreen(),
      routes: {
        '/categories_screen': (context) => CategoriesScreen(),
        '/categories_meal_screen': (context) => CategoriseMealScreen(),
        '/meal_detail_screen': (context) => MealDetailScreen(),
        // '/favorites_screen' : (context) => FavoritesScreen(),
        '/tab_screen': (context) => TabScreen(),
        '/filters_screen': (context) => FiltersScreen(),
      },
    );
  }
}
