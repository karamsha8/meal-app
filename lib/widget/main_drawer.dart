import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function() tapHandler) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          ListTile(
            leading: Icon(
              icon,
              color: Colors.grey,
            ),
            title: Text(
  title,
  style: TextStyle(
  fontSize: 25,
  fontFamily: 'RobotoCondensed',
  fontWeight: FontWeight.bold,
  color: Colors.black,
  ),
  ),
  trailing: Icon(Icons.arrow_forward_ios),
            onTap: tapHandler,
          ),
          Divider(thickness: 2,endIndent: 30,indent: 30,)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: 120,
            width: double.infinity,
            // padding: EdgeInsets.only(top: 30,left: 10,),
            color: Colors.pink.withOpacity(.7),
            child: Text(
              'Drawer',
              style: TextStyle(
                  fontSize: 25,
                  fontFamily: 'RobotoCondensed',
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Meal', Icons.restaurant, () {
            Navigator.pushReplacementNamed(context, '/tab_screen');
          }),
          buildListTile('Filters', Icons.filter, () {
            Navigator.pushReplacementNamed(context, '/filters_screen');
          }),
        ],
      ),
    );
  }
}
