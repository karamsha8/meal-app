import 'package:flutter/material.dart';
import 'package:meal_app2/dummy.dart';
import 'package:meal_app2/providers/meal_provider.dart';
import 'package:provider/provider.dart';

class MealDetailScreen extends StatelessWidget {
  


  Widget buildSectionTitle(String title) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        title,
        style: TextStyle(
          fontFamily: "RobotoCondensed",
          fontStyle: FontStyle.italic,
          fontSize: 25,
        ),
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(10),
        ),
        height: 150,
        width: 300,
        child: child);
  }

  @override
  Widget build(BuildContext context) {
    
    final mealId = ModalRoute.of(context)!.settings.arguments as String;
    final selectedMeal = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);

    
    return Scaffold(
      appBar: AppBar(
        title: Text(
          selectedMeal.title,
          style: TextStyle(
            color: Colors.white,
            fontFamily: "RobotoCondensed",
            fontStyle: FontStyle.italic,
            fontSize: 25,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(
                selectedMeal.imageUrl,
                fit: BoxFit.cover,
                height: 300,
                width: double.infinity,
              ),
              buildSectionTitle('ingredients'),
              buildContainer(
                ListView.builder(
                  itemBuilder: (ctx, index) => Card(
                    color: Colors.redAccent.withOpacity(.4),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Text(selectedMeal.ingredients[index]),
                    ),
                  ),
                  itemCount: selectedMeal.ingredients.length,
                ),
              ),
              buildSectionTitle('Steps'),
              buildContainer(ListView.builder(
                itemBuilder: (ctx, index) {
                  return Column(
                    children: [
                      ListTile(
                        leading: CircleAvatar(
                          child: Text('#${index+1}'),
                          backgroundColor: Colors.pinkAccent,
                        ),
                        title: Text(selectedMeal.steps[index]),

                      ),
                      Divider(thickness: 2,),
                    ],
                  );
                },
                itemCount: selectedMeal.steps.length,
              )),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child:  Icon(
            Provider.of<MealPR>(context).isMealFavorite ? Icons.star : Icons.star_border),
        backgroundColor: Colors.pink.withOpacity(.5),
        onPressed: (){
          Provider.of<MealPR>(context,listen: false).toggleFavorite(mealId);
        },
        elevation: 4,
      ),
    );
  }
}
