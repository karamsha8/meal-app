import 'package:flutter/material.dart';
import 'package:meal_app2/dummy.dart';
import 'package:meal_app2/models/meal.dart';
import 'package:meal_app2/providers/meal_provider.dart';
import 'package:meal_app2/widget/meal_item.dart';
import 'package:provider/provider.dart';

class CategoriseMealScreen extends StatefulWidget {


  @override
  _CategoriseMealScreenState createState() => _CategoriseMealScreenState();
}

class _CategoriseMealScreenState extends State<CategoriseMealScreen> {
   late String categoryTitle;
   late List<Meal> displayedMeals;
  @override
  void didChangeDependencies() {
    final List<Meal> availableMeal = Provider.of<MealPR>(context).availableMeal;

    final routeArg =
    ModalRoute.of(context)!.settings.arguments as Map<String, Object>;
    final categoryId = routeArg['id'];
     categoryTitle = routeArg['title'].toString();
     displayedMeals =  availableMeal.where((meal) {
      return meal.categories.contains(categoryId);
    }).toList();
    super.didChangeDependencies();
  }

  void _removeMeal(String mealId){
    setState(() {
      displayedMeals.removeWhere((meal) => meal.id == mealId );
    });
  }
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text(
          categoryTitle.toString(),
          style: TextStyle(
            color: Colors.white,
            fontFamily: "RobotoCondensed",
            fontStyle: FontStyle.italic,
            fontSize: 27,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id:displayedMeals[index].id,
            title: displayedMeals[index].title,
            imageUrl:displayedMeals[index]. imageUrl,
            duration:displayedMeals[index]. duration,
            complexity: displayedMeals[index].complexity,
            affordability:displayedMeals[index]. affordability,
          );
        },
        itemCount: displayedMeals.length,
      ),
    );
  }
}
