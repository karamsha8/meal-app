import 'package:flutter/material.dart';
import 'package:meal_app2/models/meal.dart';
import 'package:meal_app2/providers/meal_provider.dart';
import 'package:meal_app2/widget/meal_item.dart';
import 'package:provider/provider.dart';
class FavoritesScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final List<Meal> favoriteMeal= Provider.of<MealPR>(context).favoritesMeal;

    if(favoriteMeal.isEmpty){
     return Center(
       child: Text("You have no favorites yet - start adding some!"),

     );
   }
   else{
     return ListView.builder(
       itemBuilder: (ctx, index) {
         return MealItem(
           id: favoriteMeal[index].id ,
           title: favoriteMeal[index].title,
           imageUrl: favoriteMeal[index].imageUrl,
           duration: favoriteMeal[index].duration,
           complexity: favoriteMeal[index].complexity,
           affordability: favoriteMeal[index].affordability,

         );
       },
       itemCount: favoriteMeal.length,
     );

   }
  }
}
