import 'package:flutter/material.dart';
import 'package:meal_app2/models/meal.dart';
import 'package:meal_app2/screen/categories_screen.dart';
import 'package:meal_app2/screen/favorites_screen.dart';
import 'package:meal_app2/widget/main_drawer.dart';

class TabScreen extends StatefulWidget {



  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {

  int _selectedPageIndex = 0;
  late List< Map<String,Object>> _page ;
  @override
  void initState() {
    _page =[
      {'page' :CategoriesScreen(),'title':'Categories'},
      {'page' :FavoritesScreen(),'title':'Your Favorites'},
    ];
    super.initState();
  }
  void _selectPage(int value){
    setState(() {
      _selectedPageIndex  = value;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          _page[_selectedPageIndex]['title'].toString(),
          style: TextStyle(
            color: Colors.white,
            fontFamily: "RobotoCondensed",
            fontStyle: FontStyle.italic,
            fontSize: 27,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPageIndex,
        backgroundColor: Colors.pink,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Category',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Favorites',
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: _page[_selectedPageIndex]['page']as Widget,
      ),
      drawer: MainDrawer(),
    );
  }
}
