import 'package:flutter/material.dart';
import 'package:meal_app2/providers/meal_provider.dart';
import 'package:meal_app2/widget/main_drawer.dart';
import 'package:provider/provider.dart';

class FiltersScreen extends StatefulWidget {




  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutenFree = false;

  bool _lactoseFree = false;

  bool _vegan = false;

  bool _vegetarian = false;
  @override
  initState(){
    final Map<String, bool> currentFilters= Provider.of<MealPR>(context,listen: false).filters;

    _glutenFree = currentFilters['gluten'] as bool;
    _lactoseFree =currentFilters['lactose'] as bool;
    _vegan =  currentFilters['vegan'] as bool;
    _vegetarian =  currentFilters['vegetarian'] as bool;
    super.initState();
  }

  Widget buildSwitchListTile(String title, String description,
      bool currentValue, Function(bool) updateValue) {
    return SwitchListTile(
        activeColor: Colors.red,
        title: Text(title),
        subtitle: Text(description),
        value: currentValue,
        onChanged: updateValue,
        );
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, bool> currentFilters= Provider.of<MealPR>(context,listen: true).filters;

    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text(
          'Your Filters',
          style: TextStyle(
            color: Colors.white,
            fontFamily: "RobotoCondensed",
            fontStyle: FontStyle.italic,
            fontSize: 27,
          ),
        ),
        actions: [
          IconButton(
            icon:Icon(Icons.save),
            onPressed: (){
              final Map<String,bool> selectedFilter = {
                'gluten':_glutenFree,
                'lactose':_lactoseFree,
                'vegan':_vegan,
                'vegetarian':_vegetarian,
              };
              Provider.of<MealPR>(context,listen: false).setFilters(selectedFilter);

            },
          ),
        ],
        backgroundColor: Colors.pink,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              'Adjust your meal selection',
              style: TextStyle(
                fontSize: 25,
                fontFamily: 'Raleway',
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                buildSwitchListTile('Gluten-free',
                    'Only include gluten-free meals', _glutenFree, (newValue) {
                  setState(() {
                    _glutenFree = newValue;
                  });
                }),
                buildSwitchListTile(
                    'Lactose-free',
                    'Only include lactose-free meals',
                    _lactoseFree, (newValue) {
                  setState(() {
                    _lactoseFree = newValue;
                  });
                }),
                buildSwitchListTile(
                    'Vegan-free', 'Only include vegan meals', _vegan,
                    (newValue) {
                  setState(() {
                    _vegan = newValue;
                  });
                }),
                buildSwitchListTile('Vegetarian-free',
                    'Only include vegetarian meals', _vegetarian, (newValue) {
                  setState(() {
                    _vegetarian = newValue;
                  });
                }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
